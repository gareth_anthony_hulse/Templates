#include <GLFW/glfw3.h>
#include <iostream>
#include <unistd.h>
#include <cmath>

int
main ()
{  
  GLFWwindow *window;
  if (!glfwInit ())
    {
      std::cerr << "GLFW failed to initalise!" << std::endl;
      exit (EXIT_FAILURE);
    }

  GLFWmonitor *monitor = glfwGetPrimaryMonitor ();
  
  window = glfwCreateWindow (800, 600, PACKAGE, nullptr, nullptr);
  if (!window)
    {
      glfwTerminate ();
      std::cerr << "Failed to create window!" << std::endl;
      exit (EXIT_FAILURE);
    }
  
  glfwMakeContextCurrent (window);

  /* Render loop. */
  while (!glfwWindowShouldClose (window))
    {
      glClearColor (0, 0, 0, 0);
      glClear (GL_COLOR_BUFFER_BIT);
      
      glfwSwapBuffers (window);
      glfwPollEvents ();
    }

  glfwTerminate ();
}

#include <QCoreApplication>
#include <QTextStream>

QTextStream& qStdOut()
{
  static QTextStream cout(stdout);
  return cout;
}

int main(int argc, char *argv[])
{
  QCoreApplication app(argc, argv);
  qStdOut() << "Hello, world!" << endl;

  return 0;
}


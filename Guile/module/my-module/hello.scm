(define-module (my-module hello)
  #:export (hello-world))

(define (hello-world)
  (display "Hello, World!\n"))


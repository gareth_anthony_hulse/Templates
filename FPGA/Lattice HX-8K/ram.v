module ram (din, write_en, waddr, clk, raddr, dout);//512x8
   parameter addr_width = 9;
   parameter data_width = 15;
   input [addr_width-1:0] waddr, raddr;
   input [data_width-1:0] din;
   input		  write_en, clk;
   output reg [data_width-1:0] dout;
   reg [data_width-1:0]	       mem [(1<<addr_width)-1:0];
   always @(posedge clk) // Write memory.
     begin
	if (write_en)
	  mem[waddr] <= din; // Using write address bus.
     end
   always @(posedge clk) // Read memory.
     begin
	dout <= mem[raddr]; // Using read address bus.
     end
endmodule

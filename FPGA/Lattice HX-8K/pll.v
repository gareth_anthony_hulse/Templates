module pll (input clk, output clkout, lock);
   SB_PLL40_CORE #(.PLLOUT_SELECT ("GENCLK"),
		   .FEEDBACK_PATH ("SIMPLE"),
		   .DIVR (0),
		   .DIVF (60),
		   .DIVQ (1),
		   .FILTER_RANGE (5))
   
   uut (.LOCK (lock),
	.RESETB (1'b1),
	.BYPASS (1'b0),
	.REFERENCECLK (clk),
	.PLLOUTCORE (clkout));
endmodule
